#!/bin/bash

apk update 
apk upgrade
which bash || apk add bash coreutils

#echo detecting group or adding
#deluser xfs 2>/dev/null || true && delgroup xfs 2>/dev/null  ||true && sed 's/^xfs:.\+//g' /etc/passwd /etc/shadow /etc/passwd- /etc/group -i
#delgroup www-data || true && 
#addgroup www-data -g 33  && grep 33 /etc/passwd /etc/shadow 
#adduser www-data --disabled-password --uid 33 || ( delgroup www-data;deluser www-data ; adduser www-data --disabled-password --uid 33   )


#mygid=33
#env |grep -e COMPOSE_ROOT >/etc/outerenv
#echo "mygid"$mygid

#cat /etc/passwd /etc/shadow
#echo "adding user to group"


#adduser www-data $mygrpname

sed 's/www-data:x:33:33:Linux User,,,:\/home\/www-data:\/bin\/ash/www-data:x:33:33:Linux User,,,:\/var\/www:\/bin\/bash/g' /etc/passwd -i
sed 's/xfs:x:33:33:X Font Server.\+/www-data:x:33:33:Linux User,,,:\/var\/www:\/bin\/bash/g' /etc/passwd -i
test -e /var/www/html || mkdir -p /var/www/html
test -d  /etc/ssh_key_registry || mkdir  /etc/ssh_key_registry
chgrp www-data /etc/ssh_key_registry
chmod g+rw /etc/ssh_key_registry -R
chmod go-rw -R /var/www/html 
chgrp www-data /dev/stderr /dev/stdout ; 
chmod g+w /dev/stderr /dev/stdout ; 
chmod u+x  /var/www/html ; 
#chmod u-w -R /var/www/html ; 
chmod u+r -R /var/www/html ; 
chown -R www-data:www-data /var/www/html

env|grep '^SSH_PORT=[0-9].*$' && {


echo "DROPBEAR:"
which dropbear || apk add dropbear openssh-sftp-server

CONF_DIR="/etc/dropbear"
SSH_KEY_DSS="${CONF_DIR}/dropbear_dss_host_key"
SSH_KEY_RSA="${CONF_DIR}/dropbear_rsa_host_key"
SSH_KEY_ECDSA="${CONF_DIR}/dropbear_ecdsa_host_key"
SSH_KEY_ED25519="${CONF_DIR}/dropbear_ed25519_host_key"

# Check if conf dir exists
if [ ! -d ${CONF_DIR} ]; then
    mkdir -p ${CONF_DIR}
fi

chown root:root ${CONF_DIR}
chmod 755 ${CONF_DIR}


# Check if keys exists
if [ ! -f ${SSH_KEY_RSA} ]; then
    dropbearkey  -t rsa -f ${SSH_KEY_RSA} -s 4096
    chown root:root        ${SSH_KEY_RSA}
    chmod 600              ${SSH_KEY_RSA}
fi & 


## OpenSSH 7.0 and greater similarly disables the ssh-dss (DSA) public key algorithm. It too is weak and we recommend against its use. 
rm ${SSH_KEY_DSS} 2>/dev/null || true &
## Check if keys exists
#if [ ! -f ${SSH_KEY_DSS} ]; then
#    dropbearkey  -t dss -f   ${SSH_KEY_DSS};    chown root:root          ${SSH_KEY_DSS};    chmod 600                ${SSH_KEY_DSS} 
#fi &


if [ ! -f ${SSH_KEY_ED25519} ]; then
    dropbearkey --help 2>&1 |grep ed255 && ( 
        dropbearkey  -t ed25519 -f ${SSH_KEY_ED25519}
        chown root:root          ${SSH_KEY_ED25519}
        chmod 600                ${SSH_KEY_ED25519}  )
  
fi &  

if [ ! -f ${SSH_KEY_ECDSA} ]; then
    dropbearkey  -t ecdsa -f ${SSH_KEY_ECDSA} -s 521
    chown root:root          ${SSH_KEY_ECDSA}
    chmod 600                ${SSH_KEY_ECDSA}

fi &

test -e /usr/libexec/sftp-server || (mkdir -p /usr/libexec/ && ln -s /usr/lib/ssh/sftp-server /usr/libexec/sftp-server)

########### WEBROOT / DROPBEAR / PERMISSION HUSSLE 
test -d /var/www/.ssh || ( mkdir /var/www/.ssh ;chown www-data:www-data /var/www/.ssh;touch /var/www/.ssh/authorized_keys;chmod 0600 /var/www/.ssh/authorized_keys /var/www/.ssh )
test -f /var/www/.ssh/authorized_keys || touch /var/www/.ssh/authorized_keys
test -f /var/www/.ssh/authorized_keys && chown www-data:www-data /var/www/.ssh/authorized_keys
test -f /var/www/.ssh/authorized_keys && ( chmod 600 /var/www/.ssh/authorized_keys ;chmod ugo-w /var/www/.ssh/authorized_keys) 
test -d /var/www/.ssh && (chown www-data:www-data /var/www/.ssh ;chmod u+x /var/www/.ssh) &
test -d /root/.ssh || ( mkdir /root/.ssh;touch /root/.ssh/authorized_keys ; chmod 0600 /root/.ssh /root/.ssh/authorized_keys ) &


while true ;do dropbear --help 2>&1 |grep -q ^-m && /usr/sbin/dropbear -j -k -s -g -m -E -F 2>&1 
                dropbear --help 2>&1 |grep -q ^-m || /usr/sbin/dropbear -j -k -s -g    -E -F  2>&1 ;sleep 3 ; done &
echo "DROPBEAR_SPAWNED" ; } ;

rm /tmp/access.log.fifo 2>/dev/null;mkfifo /tmp/access.log.fifo
rm /tmp/errors.log.fifo 2>/dev/null;mkfifo /tmp/errors.log.fifo

while (true);do cat /tmp/access.log.fifo | stdbuf -o0 grep  -v -e 'StatusCabot' -e '"cabot/' -e '"HEAD / HTTP/1.1" 200 - "-" "curl/' -e "Uptime-Kuma" -e "UptimeRobot/" -e "docker-health-check/over9000" -e "/favicon.ico"  ;sleep 2;done &
while (true);do cat /tmp/errors.log.fifo | stdbuf -o0 grep  -v -e 'StatusCabot' -e '"cabot/' -e '"HEAD / HTTP/1.1" 200 - "-" "curl/' -e "Uptime-Kuma" -e "UptimeRobot/" -e "docker-health-check/over9000" -e "/favicon.ico"  1>&2 ;sleep 2;done &

while true ;do
  #set in config: nginx -g 'daemon off;' 
  nginx ;
  sleep 3 ; done 
