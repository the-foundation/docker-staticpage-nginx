# Docker Staticpage Nginx

Docker static page template based on nginx-alpine

#### multi-template adds www.


---
## .env Variables:
```bash
APP_URL=
STORAGE_ROOT=/storage_global/data/
LETSENCRYPT_EMAIL=
```

##### optional
```bash
DOCKER_LOG_MAX_FILE=11
DOCKER_LOG_MAX_SIZE=10m
DOCKER_LOG_DRIVER=json-file
```
---
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-staticpage-nginx/README.md/logo.jpg" width="480" height="270"/></div></a>
